package vessla.demo.openidconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoOpenidconnectApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoOpenidconnectApplication.class, args);
	}
}
