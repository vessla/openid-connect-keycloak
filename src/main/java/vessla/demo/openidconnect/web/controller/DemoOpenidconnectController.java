package vessla.demo.openidconnect.web.controller;

import java.security.Principal;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/openidconnect")
public class DemoOpenidconnectController {

	@GetMapping("/hello")
	public String sayHello(Principal principal){
		return "Hello "+principal.getName()+"!";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request, Principal principal) throws ServletException{
		request.logout();
		return "Bye "+principal.getName()+"!";
	}
}
