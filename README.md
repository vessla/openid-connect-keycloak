# README #

### What is this repository for? ###

This repository contains a simple example showing how to integrate OpenID Connect local server (Keycloak) with a Spring Boot application.

The application configuration is based on the following tutorial: https://www.youtube.com/watch?v=3I4TXPxCCVE
(although it provides the setting for confidential, not public client)

Current version of the Keycloak->Spring integration tools does not support Spring Boot 2.0.0 out of the box.

WARNING: The aplication is not production-ready (eg. client secret in application.properties is not encrypted)

### Keycloak server setup ###

The application requires the Keycloak server to be running locally on port 8080. The project contains Docker Compose configuration that helps running the server with the default settings (see: [this folder](https://bitbucket.org/vessla/demoopenidconnect_keycloak/src/b318e04986b6666b4866e015838aa098945d098c/keycloak/?at=master)).

Runnig and configuring the server requires the following steps:

1. Run the 'docker-compose up -d keycloak' command

2. If you are running the server for the first time, add the admin user and restart the Keycloak server:

```
	docker exec -it keycloak_keycloak_1 /bin/bash

	[jboss@ce40ad87e85e keycloak]$ cd keycloak/bin
	[jboss@ce40ad87e85e bin]$ ./add-user-keycloak.sh -u admin
	Press ctrl-d (Unix) or ctrl-z (Windows) to exit
	Password:
	Added 'admin' to '/opt/jboss/keycloak/standalone/configuration/keycloak-add-user.json', restart server to load user
	[jboss@ce40ad87e85e bin]$ exit
	exit

	docker-compose restart keycloak
```

At this point you can connect to the server using the following URL: http://localhost:8080

WARNING: The provided configuration is not production-ready (eg. the PostgreSQL password is not stored in a secure manner)

### Some resources regarding OpenID Connect ###

https://www.youtube.com/watch?v=1ZX7554l8hY

### Who do I talk to? ###

pjadamska@gmail.com