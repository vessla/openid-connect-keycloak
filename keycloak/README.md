## Setup

```
docker-compose build
```

## Start

```
docker-compose up -d keycloak
```

Open a browser on `http://localhost:8080` 

When running for the first time, admin user needs to be added. 
Currently keycloak does not seem to recognize connection to docker via browser as local connection 
(registration form is not displayed), so it is necessary to log into the keycloak container and run
add-user-keycloak.sh script:

```
docker exec -it keycloak_keycloak_1 /bin/bash
[jboss@ce40ad87e85e keycloak]$ cd keycloak/bin
[jboss@ce40ad87e85e bin]$ ./add-user-keycloak.sh -u admin
Press ctrl-d (Unix) or ctrl-z (Windows) to exit
Password:
Added 'admin' to '/opt/jboss/keycloak/standalone/configuration/keycloak-add-user.json', restart server to load user
[jboss@ce40ad87e85e bin]$ exit
exit

docker-compose restart keycloak
```